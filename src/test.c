#include "test.h"
#define HEAP_START_SIZE (4096)

static void messages(bool error, const char *messages, const char *test_name)
{
    error ? fprintf(stderr, "%s in test: %s", messages, test_name) : printf("%s", messages);
}

static struct block_header *block_get_header(void *contents)
{
    return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

static size_t size = 123;

static void test_1() {
    char *test = "malloc_1";
    void *tested_block = _malloc(size);
    printf("Test 1 (malloc)");
    tested_block == NULL ? messages(true, "block is Null", test) : messages(false, "block is not Null", test);
    block_get_header(tested_block)->capacity.bytes < size ? messages(true, "capacity is not enough", test) : messages(false, "capacity is enough", test);
    block_get_header(tested_block)->is_free ? messages(true, "mistakenly marked as free", test) : messages(false, "block marked as not free", test);
}


static void test_2() {
    char *test = "free_1";
    void *tested_block_1 = _malloc(size);
    void *tested_block_2 = _malloc(size);
    void *tested_block_3 = _malloc(size);
    void *tested_block_4 = _malloc(size);
    void *tested_block_5 = _malloc(size);
    (tested_block_1 != NULL && tested_block_2 != NULL && tested_block_3 != NULL 
    && tested_block_4 != NULL && tested_block_5 != NULL) ? messages(false, "block is not Null", test) : 
    messages(true, "some block is Null", test);
    _free(tested_block_2);
}

static void test_3() {
    char *test = "free_2";
    void *tested_block_1 = _malloc(size);
    void *tested_block_2 = _malloc(size);
    void *tested_block_3 = _malloc(size);
    void *tested_block_4 = _malloc(size);
    void *tested_block_5 = _malloc(size);
    (tested_block_1 != NULL && tested_block_2 != NULL && tested_block_3 != NULL && tested_block_4 != NULL && tested_block_5 != NULL) ? messages(false, "block is not Null", test) : messages(true, "some block is Null", test);
    _free(tested_block_2);
    _free(tested_block_3);
}

static void test_4(void* beginning) {
    char *test = "malloc_2";
    size_t size = HEAP_START_SIZE + 10;
    void *tested_block = _malloc(size);
    tested_block == NULL ? messages(true, "block is Null", test) : messages(false, "block is not null", test);
    (void*) block_get_header(tested_block) > (void*) (beginning + HEAP_START_SIZE) ? messages(true, "block not in old region", test) :
    messages(false, "block in old region", test);
    _free(tested_block);
}


int run_test() {
    void *beginning = heap_init(HEAP_START_SIZE);
    if (beginning == NULL) {
        fprintf(stderr, "wrong heap init");
    }
    debug_heap(stderr, beginning);
    printf("Test 1 (default malloc)");
    test_1();
    debug_heap(stderr, beginning);
    printf("Test 2 (free(1) one block from several)");
    test_2();
    debug_heap(stderr, beginning);
    printf("Test 3 (free(2) several blocks from several)");
    test_3();
    debug_heap(stderr, beginning);
    printf("Test 4 (malloc(2) malloc when there is no free space)");
    test_4(beginning);
    debug_heap(stderr, beginning);
    return 0;
}