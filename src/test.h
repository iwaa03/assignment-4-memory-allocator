#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

int run_test();

#endif 